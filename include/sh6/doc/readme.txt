Using the HTML version of the SmartHeap 6 Programmer's Guide
------------------------------------------------------------

Unzip sh6ug.zip to a directory of your choice. Typical usage would be to open the file TOC.htm in your browser, find the topic you're interested in and follow the link. 

please report any problems to support@microquill.com.

thanks!