/*** Defines ***/

/* File Menu */
#define IDM_EXIT        100
/* List Menu */
#define IDM_CREATE      200
#define IDM_FREE        210
#define IDM_LENGTH      220
#define IDM_REVERSE     230
#define IDM_CHECK       232
/* Link Menu */
#define IDM_INSERT      300
#define IDM_APPEND      310
#define IDM_PUSH        320
#define IDM_POP         330
#define IDM_DELETE      340
#define IDM_NTH         350
#define IDM_LAST        360
/* Value Menu */
#define IDM_FIND        400
#define IDM_POSITION    410
#define IDM_DELETE_VALUE 420
#define IDM_DELETE_OCCURRENCES 430
/* Error Menu */
#define IDM_BAD_POOL    500
#define IDM_BAD_POINTER 510
#define IDM_DOUBLE_FREE 520
#define IDM_ALLOC_ZERO 540
/* Help Menu */
#define IDM_ABOUT       700

/* Child Window Controls */
#define IDC_LISTBOX     1000
#define IDC_EDIT        1100
#define IDC_TEXT        1200

/* Icon */
#define IDI_LIST        2000
