/* shmalmac.c -- SmartHeap(tm) macro version of malloc/calloc/realloc/free.
 *
 * Copyright (C) 1991-1996 Compuware Corporation.
 * All Rights Reserved.
 *
 * No part of this source code may be copied, modified or reproduced
 * in any form without retaining the above copyright notice.
 * This source code, or source code derived from it, may not be redistributed
 * without express written permission of the author.
 *
 */

#define MALLOC_MACRO 1
#include "shmalloc.c"
