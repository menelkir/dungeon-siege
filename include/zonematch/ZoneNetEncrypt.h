#ifndef _ZONENETENCRYPT_H
#define _ZONENETENCRYPT_H

#include "zonenet.h"

// {7B3582D3-7CCB-457d-A503-C19C91989FBE}
DEFINE_GUID(CLSID_ZoneNetEncrypt, 0x7b3582d3, 0x7ccb, 0x457d, 0xa5, 0x3, 0xc1, 0x9c, 0x91, 0x98, 0x9f, 0xbe);

// {B94E466F-10EF-419f-98C6-33364F49A843}
DEFINE_GUID(IID_IZoneNetEncrypt, 0xb94e466f, 0x10ef, 0x419f, 0x98, 0xc6, 0x33, 0x36, 0x4f, 0x49, 0xa8, 0x43);

interface IZoneNetEncrypt : public IUnknown
{
    //
    // GenerateKey
    //
    // converts a given string to a key which may used for decryption or encryption
    //
    // pKey->cbSize must be set to sizeof(ZONETICKET_KEY) prior to make the call
    //
    virtual HRESULT STDMETHODCALLTYPE GenerateKey
                     ( IN     LPCSTR          pszString,	// string to generate key from
                       IN OUT ZoneNetEncryptionKey* pKey	// key to use for decryption or encryption
                     ) = 0;

    //
    // Encrypt
    //
    // encrypts an encrypted ZONETICKET for use
    //
    virtual HRESULT STDMETHODCALLTYPE EncryptData
                     ( IN     const ZoneNetEncryptionKey* pKey,	// Key generated by GenerateKey()
                       IN     const LPBYTE          pSrcData,	// buffer containing data
                       IN           DWORD           cbSrcData,	// sizeof data pointed to by pBuffer
                       IN OUT       LPBYTE*			pDestData,	// encrypted data
                       IN OUT       DWORD*          pcbDestData	// size of encrypted data
                     ) = 0;

    virtual HRESULT STDMETHODCALLTYPE Free(
                       IN     const LPBYTE          pSrcData,   // pointer to data to be freed
                       IN     const DWORD           cbData      // amount of data to be freed
                     ) = 0;
};

#endif //ndef _ZONENETENCRYPT_H
