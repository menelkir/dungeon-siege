#ifndef _ZONENETDECRYPT_H
#define _ZONENETDECRYPT_H

#include "zonenet.h"

// {B7618931-7D90-4474-B9F3-66DA485F8CA3}
DEFINE_GUID(CLSID_ZoneNetDecrypt, 0xb7618931, 0x7d90, 0x4474, 0xb9, 0xf3, 0x66, 0xda, 0x48, 0x5f, 0x8c, 0xa3);

// {6814FB18-60D9-41d7-BCB4-68E326B52CDA}
DEFINE_GUID(IID_IZoneNetDecrypt,  0x6814fb18, 0x60d9, 0x41d7, 0xbc, 0xb4, 0x68, 0xe3, 0x26, 0xb5, 0x2c, 0xda);

interface IZoneNetDecrypt : public IUnknown
{
    //
    // GenerateKey
    //
    // converts a given string to a key which may used for decryption or encryption
    //
    // pKey->cbSize must be set to sizeof(ZONETICKET_KEY) prior to make the call
    //
    virtual HRESULT STDMETHODCALLTYPE GenerateKey
                     ( IN     LPCSTR          pszString,	// string to generate key from
                       IN OUT ZoneNetEncryptionKey* pKey	// key to use for decryption or encryption
                     ) = 0;

    //
    // Decrypt
    //
    // decrypts an encrypted ZONETICKET for use
    //
    virtual HRESULT STDMETHODCALLTYPE DecryptData
                     ( IN     const ZoneNetEncryptionKey* pKey,	// Key generated by GenerateKey()
                       IN     const LPBYTE          pSrcData,	// buffer containing data
                       IN           DWORD           cbSrcData,	// sizeof data pointed to by pBuffer
                       IN OUT       LPBYTE*			pDestData,	// decrypted data
                       IN OUT       DWORD*          pcbDestData	// size of decrypted data
                     ) = 0;

    virtual HRESULT STDMETHODCALLTYPE Free(
                       IN     const LPBYTE          pSrcData,   // pointer to data to be freed
                       IN     const DWORD           cbData      // amount of data to be freed
                     ) = 0;
};

#endif //ndef _ZONENETDECRYPT_H
