//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by ContentTuneRes.rc
//
#define IDD_CONTENTTUNE                 101
#define IDC_PROGRESS                    1000
#define IDC_EXPORT                      1001
#define IDC_CLOSE                       1002
#define IDC_IMPORT                      1003
#define IDC_STATUS                      1004
#define IDC_HOMEPATH                    1005
#define IDC_FILENAME                    1006
#define IDC_FUELROOT                    1007
#define IDC_SPECIALIZED                 1008
#define IDC_OPENEXCEL                   1009
#define IDC_COMBOGASFILES               1011
#define IDC_TEMP                        1012
#define IDC_FLIPTABLE                   1013
#define IDC_SHAREDKEYS                  1015

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        102
#define _APS_NEXT_COMMAND_VALUE         40001
#define _APS_NEXT_CONTROL_VALUE         1016
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
