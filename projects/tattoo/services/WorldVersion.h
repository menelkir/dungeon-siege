//////////////////////////////////////////////////////////////////////////////
//
// File     :  WorldVersion.h
// Author(s):  Scott Bilas
//
// Summary  :  Contains version info for the shared World codebase.
//
// Copyright � 1999 Gas Powered Games, Inc.  All rights reserved.
//----------------------------------------------------------------------------
//  $Revision:: $              $Date:$
//----------------------------------------------------------------------------
//////////////////////////////////////////////////////////////////////////////

#pragma once
#ifndef __WORLDVERSION_H
#define __WORLDVERSION_H

//////////////////////////////////////////////////////////////////////////////
// Configuration constants

// $ Configure this section for the auto-gen features.

// @VERSION             = 1.12
// @SPECIAL_BUILD_TYPE	= INTERNAL

//////////////////////////////////////////////////////////////////////////////
// Public constants

// this is the complete name of the product and all its tools.
#define DS1_PRODUCT_NAME   "Gas Powered Games Dungeon Siege\0"
#define DS1TK_PRODUCT_NAME "Gas Powered Games Dungeon Siege Tool Kit\0"

// other misc contents
#define DS1_FOURCC_ID 'DSig'

//////////////////////////////////////////////////////////////////////////////

#endif  // __WORLDVERSION_H

//////////////////////////////////////////////////////////////////////////////
