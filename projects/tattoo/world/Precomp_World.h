//////////////////////////////////////////////////////////////////////////////
//
// File     :  precomp_world.h
// Author(s):  Scott Bilas
//
// Summary  :  Contains the headers that will be precompiled for this project.
//
// Copyright � 1999 Gas Powered Games, Inc.  All rights reserved.
//----------------------------------------------------------------------------
//  $Revision:: $              $Date:$
//----------------------------------------------------------------------------
//////////////////////////////////////////////////////////////////////////////

#ifndef __PRECOMP_WORLD_H
#define __PRECOMP_WORLD_H
#pragma once

//////////////////////////////////////////////////////////////////////////////

// from libraries
#include "gpcore.h"

// other fun
#include <list>
#include <map>
#include <set>
#include <vector>

// from world
#include "BlindCallback.h"
#include "fuel.h"
#include "gpprofiler.h"
#include "matrix_3x3.h"
#include "siege_database_guid.h"
#include "siege_pos.h"
#include "space_3.h"
#include "stringtool.h"
#include "vector_3.h"
#include "Point2.h"
#include "GoDb.h"
#include "player.h"
#include "server.h"
#include "worldmessage.h"
#include "siege_engine.h"
#include "stdhelp.h"

// experimental (to improve build times)
#include "fubipersist.h"
#include "fubitraitsimpl.h"
#include "gocore.h"
#include "gosupport.h"

//////////////////////////////////////////////////////////////////////////////

#endif  // __PRECOMP_WORLD_H

//////////////////////////////////////////////////////////////////////////////
