//////////////////////////////////////////////////////////////////////////////
//
// File     :  precomp_game.h
// Author(s):  Scott Bilas
//
// Summary  :  Contains the headers that will be precompiled for this project.
//
// Copyright � 1999 Gas Powered Games, Inc.  All rights reserved.
//----------------------------------------------------------------------------
//  $Revision:: $              $Date:$
//----------------------------------------------------------------------------
//////////////////////////////////////////////////////////////////////////////

#pragma once
#ifndef __PRECOMP_GAME_H
#define __PRECOMP_GAME_H

//////////////////////////////////////////////////////////////////////////////

// from libraries
#include "gpcore.h"

#include "BlindCallback.h"
#include "fubidefs.h"
#include "fuel.h"
#include "gpprofiler.h"
#include "siege_engine.h"
#include "siege_pos.h"
#include "stringtool.h"
#include "ui_gridbox.h"
#include "ui_itemslot.h"
#include "ui_messenger.h"
#include "ui_shell.h"
#include "ui_window.h"
#include "stdhelp.h"

// from world
#include "Go.h"
//#include "gomind.h"

//////////////////////////////////////////////////////////////////////////////

#endif  // __PRECOMP_GAME_H

//////////////////////////////////////////////////////////////////////////////
