//////////////////////////////////////////////////////////////////////////////
//
// File     :  precomp_siegenodeobject.h
// Author(s):  biddle
//
// Summary  :  Contains the headers that will be precompiled for this project.
//
// Copyright � 1999 Gas Powered Games, Inc.  All rights reserved.
//----------------------------------------------------------------------------
//  $Revision:: $              $Date:$
//----------------------------------------------------------------------------
//////////////////////////////////////////////////////////////////////////////

#pragma once
#ifndef __PRECOMP_SEIGENODEOBJECT_H
#define __PRECOMP_SEIGENODEOBJECT_H

//////////////////////////////////////////////////////////////////////////////

#define gpassert(x) 

#include "math\gpcore.h"
#include "smart_pointer.h"
#include "max.h"
#include "bmmlib.h"
#include "stdmat.h"

#include "boundary.h"
#include "enumerator.h"
#include "exporter.h"
#include "obb.h"
#include "SiegeNodeObject.h"
#include "vertnormals.h"
#include "resource.h"
#include "indexlists.h"

//////////////////////////////////////////////////////////////////////////////

#endif  // __PRECOMP_OBBTREE_H

//////////////////////////////////////////////////////////////////////////////
