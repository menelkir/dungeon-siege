//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by SiegeNodeObject.rc
//
#define IDS_LIBDESCRIPTION              1
#define IDS_CATEGORY                    2
#define IDS_CLASS_NAME                  3
#define IDS_PARAMS                      4
#define IDD_PANEL                       101
#define IDD_DOOR_SUBPANEL               102
#define IDD_IDOOR_SUBPANEL              102
#define IDD_FLOOR_SUBPANEL              103
#define IDD_OBB_SUBPANEL                104
#define IDD_SPOT_SUBPANEL               106
#define IDD_DOOR_SUBPANEL1              107
#define IDD_NORMALS_SUBPANEL            108
#define IDD_IGNORED_SUBPANEL            109
#define IDD_NORMALS_SUBPANEL1           110
#define IDD_WATER_SUBPANEL              111
#define IDC_CLOSEBUTTON                 1000
#define IDC_DOSTUFF                     1000
#define IDC_PICK_BOUNDING_MESH_BUTTON   1003
#define IDC_BOUNDING_MESH_NAME          1004
#define IDC_SIEGE_NODE_NAME             1006
#define IDC_CREATE_SIEGE_NODE           1007
#define IDC_DOOR_LOOP_COUNT             1010
#define IDC_TOPO_LOOP_COUNT             1011
#define IDC_ADD_DOOR                    1011
#define IDC_DELETE_DOOR                 1012
#define IDC_DELETE_FROM_FLOOR           1012
#define IDC_FLIP_DOOR                   1013
#define IDC_RESET_FLOOR                 1013
#define IDC_SELECTED_LOOP_NUM           1014
#define IDC_FETCH_SELECTED_FLOOR        1014
#define IDC_SELECTED_FIRST_VERT         1015
#define IDC_SELECTED_LAST_VERT          1016
#define IDC_DOOR_ID                     1017
#define IDC_ADD_TO_FLOOR                1018
#define IDC_TOTAL_FACE_NUM              1019
#define IDC_FLOOR_FACE_NUM              1020
#define IDC_SELECTED_FACE_NUM           1021
#define IDC_BUTTON1                     1021
#define IDC_FIND_SEPARATE_MESHES        1022
#define IDC_SLIDER1                     1023
#define IDC_RECURSELEVEL                1024
#define IDC_CROOT                       1026
#define IDC_ORIENT_DOOR                 1026
#define IDC_CRIGHT                      1027
#define IDC_MOVE_START_VERT             1027
#define IDC_ADD_SPOT                    1027
#define IDC_DELETE_SPOT                 1028
#define IDC_CLEFT                       1029
#define IDC_SNAP_SPOT                   1029
#define IDC_TOTAL_SPOT_NUM              1030
#define IDC_LOCK_SPOT                   1032
#define IDC_PREV_SPOT                   1038
#define IDC_NEXT_SPOT                   1039
#define IDC_CURRENT_SPOT                1040
#define IDC_CLEAR_SELECTION             1041
#define IDC_ADD_TO_IGNORED              1042
#define IDC_DELETE_FROM_IGNORED         1043
#define IDC_RESET_IGNORED               1044
#define IDC_IGNORED_NUM                 1045
#define IDC_SELECTED_IGNORED_NUM        1046
#define IDC_VERT_COUNT                  1047
#define IDC_UNLOCK_NORMS                1048
#define IDC_LOCK_NORMS                  1049
#define IDC_RESET_NORMS                 1050
#define IDC_LOCKED_NORM_NUM             1051
#define IDC_SELECTED_NORM_NUM           1053
#define IDC_SPOOF_NORMS                 1054
#define IDC_UNSPOOF_NORMS               1055
#define IDC_SPOOFED_NORM_NUM            1056
#define IDC_SELECTED_SPOOFED_NORM_NUM   1057
#define IDC_RESET_SPOOFED               1058
#define IDC_ADD_TO_WATER                1059
#define IDC_DELETE_FROM_WATER           1060
#define IDC_RESET_WATER                 1061
#define IDC_FETCH_SELECTED_WATER        1062
#define IDC_WATER_FACE_NUM              1063
#define IDC_FETCH_SELECTED_IGNORED      1064
#define IDC_COLOR                       1456
#define IDC_EDIT                        1490
#define IDC_SPIN                        1496

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        112
#define _APS_NEXT_COMMAND_VALUE         40001
#define _APS_NEXT_CONTROL_VALUE         1065
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
