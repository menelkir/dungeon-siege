//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by GUIMaster.rc
//
#define IDD_ABOUTBOX                    100
#define ID_EDIT_LEFT                    101
#define ID_EDIT_RIGHT                   102
#define ID_EDIT_UP                      103
#define ID_EDIT_DOWN                    104
#define ID_WINDOW_PROPERTIES            105
#define ID_WINDOW_BRINGTOFRONT          106
#define ID_WINDOW_MESSAGES              107
#define ID_WINDOW_COMMON_SETTINGS       108
#define ID_WINDOW_GROUP                 109
#define ID_WINDOW_TEXT                  110
#define IDR_MAINFRAME                   128
#define IDR_GUIMASTYPE                  129
#define IDD_DIALOG_CONTROLS             130
#define IDD_DIALOG_START                131
#define IDD_DIALOG_CREATE_INTERFACE     132
#define IDD_DIALOG_LOAD_INTERFACE       133
#define IDD_DIALOG_PREFERENCES          134
#define IDD_DIALOG_SHOWINTERFACES       135
#define IDD_DIALOG_PROPERTIES           136
#define IDD_DIALOG_COMMON_SETTINGS      137
#define IDD_DIALOG_GROUP                138
#define IDD_DIALOG_TEXT                 139
#define IDR_TOOLBAR_MODE                140
#define IDD_DIALOG1                     142
#define IDD_DIALOG_SET_BACKGROUND       143
#define IDC_LIST_CONTROLS               1000
#define IDC_BUTTON_CREATE               1002
#define IDC_BUTTON_LOAD                 1003
#define IDC_EDIT_NAME                   1004
#define IDC_LIST_INTERFACES             1005
#define IDC_CHECK_SNAPTOGUIDES          1006
#define IDC_BUTTON_BG_COLOR             1007
#define IDC_BUTTON_SHOW                 1009
#define IDC_BUTTON_HIDE                 1010
#define IDC_STATIC_CURRENT_INTERFACE    1011
#define IDC_BUTTON_EDIT_INTERFACE       1012
#define IDC_BUTTON_CLOSE                1013
#define IDC_BUTTON_LOADTEXTURE          1015
#define IDC_BUTTON_NOTEXTURE            1016
#define IDC_EDIT_U1                     1017
#define IDC_EDIT_V1                     1018
#define IDC_EDIT_U2                     1019
#define IDC_EDIT_V2                     1020
#define IDC_CHECK_TILED                 1021
#define IDC_EDIT_ALPHA                  1022
#define IDC_BUTTON_RESIZERECT           1023
#define IDC_BUTTON_RESIZEUVRECT         1024
#define IDC_EDIT_X1                     1025
#define IDC_EDIT_Y1                     1026
#define IDC_EDIT_X2                     1027
#define IDC_EDIT_Y2                     1028
#define IDC_CHECK_RIGHTANCHOR           1029
#define IDC_CHECK_BOTTOMANCHOR          1030
#define IDC_STATIC_TEXTURENAME          1031
#define IDC_CHECK_COMMON                1032
#define IDC_CHECK_GROUP_MOVE            1034
#define IDC_CHECK_DOCK_GROUP_MOVE       1035
#define IDC_CHECK_RADIO_GROUP_MOVE      1036
#define IDC_CHECK_PARENT_MOVE           1037
#define IDC_CHECK_DRAW_GUIDES           1038
#define IDC_CHECK_HIDE_BLANKS           1039
#define IDC_EDIT_GROUP                  1040
#define IDC_EDIT_DOCKBAR_GROUP          1041
#define IDC_EDIT_RADIO_GROUP            1042
#define IDC_COMBO_PARENT                1043
#define IDC_COMBO_FONT                  1044
#define IDC_EDIT_TEXT                   1045
#define IDC_COMBO_JUSTIFY               1046
#define IDC_BUTTON_COLOR                1047
#define IDC_EDIT_TEMPLATE               1048
#define IDC_EDIT_WIDTH                  1049
#define IDC_EDIT_HEIGHT                 1050
#define IDC_COMBO1                      1051
#define IDC_BUTTON1                     1053
#define IDC_CHECK_ALL_INTERFACES        1054
#define IDC_EDIT_BACKGROUND             1055
#define IDC_CHECK_DISABLE_RESIZE        1056
#define IDC_CHECK_DISABLE_MOVE          1057
#define IDC_EDIT_ADD                    1058
#define IDC_BUTTON_PROCESS              1059
#define IDC_BUTTON_TEST                 1060
#define ID_VIEW_CONTROLS                32771
#define ID_MODE_CREATION                32772
#define ID_MODE_STRUCTURE               32773
#define ID_MODE_UVSMARTMAP              32774
#define ID_MODE_UVEDIT                  32775
#define ID_MODE_GUIDEEDIT               32776
#define ID_FILE_OPEN_INTERFACE          32777
#define ID_FILE_SAVE_INTERFACE          32778
#define ID_FILE_NEW_INTERFACE           32779
#define ID_FILE_CLOSEINTERFACES         32780
#define ID_VIEW_PREFERENCES             32781
#define ID_VIEW_INTERFACES              32782
#define ID_EDIT_DELETE                  32783
#define ID_LOCALIZATIONTOOLS_CREATEGUIIDMAP 32791
#define ID_EDIT_SET_BACKGROUND          32792

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_3D_CONTROLS                     1
#define _APS_NEXT_RESOURCE_VALUE        144
#define _APS_NEXT_COMMAND_VALUE         32793
#define _APS_NEXT_CONTROL_VALUE         1061
#define _APS_NEXT_SYMED_VALUE           111
#endif
#endif
