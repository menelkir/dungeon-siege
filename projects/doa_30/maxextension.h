/**********************************************************************
 *<
	FILE: maxextension.h

	DESCRIPTION:	Template Utility

	CREATED BY:

	HISTORY:

 *>	Copyright (c) 1997, All Rights Reserved.
 **********************************************************************/

#ifndef __MAXEXTENSION__H
#define __MAXEXTENSION__H

#include "Max.h"
#include "resource.h"
#include "istdplug.h"


#include "frontend.h"

extern TCHAR *GetString(int id);

extern HINSTANCE hInstance;

#endif // __MAXEXTENSION__H
