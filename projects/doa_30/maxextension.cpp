#include "MAXScrpt.h"

#include "Numbers.h"
#include "Arrays.h"
#include "Name.h"
#include "3DMath.h"

#include "MAXObj.h"

#include "bipexp.h"
#include "PhyExp.H"

#include "stdio.h"

/*
#include "MAXclses.h"
#include "MSTime.h"
#include "Parser.h"
*/

Modifier* FindPhysiqueModifier (INode* nodePtr)
{
	// Get object from node. Abort if no object.
	Object* ObjectPtr = nodePtr->GetObjectRef();
	

	if (!ObjectPtr) return NULL;

	// Is derived object ?
	if (ObjectPtr->SuperClassID() == GEN_DERIVOB_CLASS_ID)
	{
		// Yes -> Cast.
		IDerivedObject* DerivedObjectPtr = static_cast<IDerivedObject*>(ObjectPtr);

		// Iterate over all entries of the modifier stack.
		int ModStackIndex = 0;
		while (ModStackIndex < DerivedObjectPtr->NumModifiers())
		{
			// Get current modifier.
			Modifier* ModifierPtr = DerivedObjectPtr->GetModifier(ModStackIndex);

			// Is this Physique ?
			if (ModifierPtr->ClassID() == Class_ID(PHYSIQUE_CLASS_ID_A, PHYSIQUE_CLASS_ID_B))
			{
				// Yes -> Exit.
				return ModifierPtr;
			}

			// Next modifier stack entry.
			ModStackIndex++;
		}
	}

	// Not found.
	return NULL;
}


#include "definsfn.h"

//*********************************************************

void DOA_init(void) {

	mputs("DOA - experimental MAXScript support for CStudio loaded\n"); 

}
 
//*********************************************************
//*********************************************************
//*********************************************************

def_visible_primitive( doaHello,				"doaHello");

def_visible_primitive( doaValidMesh,				"doaValidMesh");
def_visible_primitive( doaNumVerts,					"doaNumVerts");
def_visible_primitive( doaNumBonesAttachedToVert,	"doaNumBonesAttachedToVert");

def_visible_primitive( doaGetWeightByBoneNode,	"doaGetWeightByBoneNode");
def_visible_primitive( doaGetWeightByBoneIndex,	"doaGetWeightByBoneIndex");
def_visible_primitive( doaGetTotalWeight,		"doaGetTotalWeight");
def_visible_primitive( doaGetBoneByVertexIndex,	"doaGetBoneByVertexIndex");

def_visible_primitive( doaValidBone,			"doaValidBone");
def_visible_primitive( doaValidFootstep,		"doaValidFootstep");
def_visible_primitive( doaValidBody,			"doaValidBody");

def_visible_primitive( doaValidRigidVertex,		"doaValidRigidVertex");

def_visible_primitive( doaNumTMKeys,			"doaNumTMKeys");
def_visible_primitive( doaNumHorizontalKeys,	"doaNumHorizontalKeys");
def_visible_primitive( doaNumVerticalKeys,		"doaNumVerticalKeys");
def_visible_primitive( doaNumRotationKeys,		"doaNumRotationKeys");

def_visible_primitive( doaGetTMKeyTimes,			"doaGetTMKeyTimes");
def_visible_primitive( doaGetHorizontalKeyTimes,	"doaGetHorizontalKeyTimes");
def_visible_primitive( doaGetVerticalKeyTimes,		"doaGetVerticalKeyTimes");
def_visible_primitive( doaGetRotationKeyTimes,		"doaGetRotationKeyTimes");

def_visible_primitive( doaGetNextKeyTime,			"doaGetNextKeyTime");

def_visible_primitive( doaSetKeyTMValue,			"doaSetKeyTMValue");

def_visible_primitive( doaUniformMatrix,			"doaUniformMatrix");
def_visible_primitive( doaRelativeMatrix,			"doaRelativeMatrix");
def_visible_primitive( doaAbsoluteMatrix,			"doaAbsoluteMatrix");

def_visible_primitive( doaStringToInt,				"doaStringToInt");
def_visible_primitive( doaIntToName,				"doaIntToName");

//*********************************************************

Value* doaHello_cf(Value** arg_list, int count) {

	mputs("Hello, Kirkland!\n"); 
	return &true_value;

}

//*********************************************************

Value* doaValidMesh_cf(Value** arg_list, int count) {

	check_arg_count(doaValidMesh, 1, count);

	INode* nodeptr = arg_list[0]->to_node();

	if (FindPhysiqueModifier(nodeptr)) {
		return &true_value;
	}
	
	return &false_value;
}

//*********************************************************

Value* doaNumVerts_cf(Value** arg_list, int count) {

	check_arg_count(doaNumVerts, 1, count);

	INode* nodeptr = arg_list[0]->to_node();

	Modifier* phy;

	if (! (phy = FindPhysiqueModifier(nodeptr)) ) {
		// Could raise an exception?
		return Integer::intern(0); 
	}

	IPhysiqueExport *phyExp = (IPhysiqueExport *)phy->GetInterface(I_PHYINTERFACE);

	IPhyContextExport *phyContextExp = (IPhyContextExport *)phyExp->GetContextInterface(nodeptr);

	int numverts = phyContextExp->GetNumberVertices();

	phyExp->ReleaseContextInterface(phyContextExp);

	return Integer::intern(numverts); 


}

//*********************************************************

Value* doaNumBonesAttachedToVert_cf(Value** arg_list, int count) {

	check_arg_count(doaNumBonesAttachedToVert, 2, count);

	INode* mesh = arg_list[0]->to_node();
	int vert = arg_list[1]->to_int();

	vert--;		// don't get all off-by-one coming out of MAXSCRIPT!

	Modifier* phy;

	int numbones = 0;

	if (! (phy = FindPhysiqueModifier(mesh)) ) {
		// Could raise an exception?
		return &false_value;
	}

	IPhysiqueExport *phyExp = (IPhysiqueExport *)phy->GetInterface(I_PHYINTERFACE);

	if ( IPhyContextExport *phyContextExp = (IPhyContextExport *)phyExp->GetContextInterface(mesh) ) {

		phyContextExp->ConvertToRigid(true);
		phyContextExp->AllowBlending(true);

		int numverts = phyContextExp->GetNumberVertices();

		if (vert < 0 || numverts <= vert) {
			// This deserves an ERROR!
			phyExp->ReleaseContextInterface(phyContextExp);
			return &false_value;
		}


		if (IPhyVertexExport *phyVertExp = (IPhyVertexExport *)phyContextExp->GetVertexInterface(vert) ) {

			int vtype = phyVertExp->GetVertexType();

			if (vtype & BLENDED_TYPE) {

				IPhyBlendedRigidVertex* phyBlendVert = (IPhyBlendedRigidVertex *)phyVertExp;

				numbones =  phyBlendVert->GetNumberNodes();

			} else {

				numbones =  1;

			}

			phyContextExp->ReleaseVertexInterface(phyVertExp);

		} else {
			// This deserves an ERROR!
			phyExp->ReleaseContextInterface(phyContextExp);
			return &false_value;
		}


		phyExp->ReleaseContextInterface(phyContextExp);

	}


	return Integer::intern(numbones); 


}

//*********************************************************

Value* doaGetWeightByBoneNode_cf(Value** arg_list, int count) {

	check_arg_count(doaGetWeightByBoneNode, 3, count);

	INode* mesh = arg_list[0]->to_node();
	INode* bone = arg_list[1]->to_node();
	int vert = arg_list[2]->to_int();

	vert--;		// don't get all off-by-one coming out of MAXSCRIPT!

	Modifier* phy;
	float weight = 0.0;

	if (! (phy = FindPhysiqueModifier(mesh)) ) {
		// Could raise an exception?
		return &false_value;
	}

	IPhysiqueExport *phyExp = (IPhysiqueExport *)phy->GetInterface(I_PHYINTERFACE);

	if ( IPhyContextExport *phyContextExp = (IPhyContextExport *)phyExp->GetContextInterface(mesh) ) {

		phyContextExp->ConvertToRigid(true);
		phyContextExp->AllowBlending(true);

		int numverts = phyContextExp->GetNumberVertices();

		if (vert < 0 || numverts <= vert) {
			// This deserves an ERROR!
			phyExp->ReleaseContextInterface(phyContextExp);
			return &false_value;
		}


		if (IPhyVertexExport *phyVertExp = (IPhyVertexExport *)phyContextExp->GetVertexInterface(vert) ) {

			int vtype = phyVertExp->GetVertexType();

			if (vtype & BLENDED_TYPE) {

				IPhyBlendedRigidVertex* phyBlendVert = (IPhyBlendedRigidVertex *)phyVertExp;

				for (int b = 0; b < phyBlendVert->GetNumberNodes(); b++) {


					if (phyBlendVert->GetNode(b) == bone) {
						weight += phyBlendVert->GetWeight(b);
						// break;  The bone may appear more than once!!!!
					}
				}

			} else {

				IPhyRigidVertex* phyRigidVert = (IPhyRigidVertex *)phyVertExp;
				if (phyRigidVert->GetNode() == bone) {
					weight = 1.0;
				}

			}

			phyContextExp->ReleaseVertexInterface(phyVertExp);

		} else {
			// This deserves an ERROR!
			phyExp->ReleaseContextInterface(phyContextExp);
			return &false_value;
		}


		phyExp->ReleaseContextInterface(phyContextExp);

	}


	return Float::intern(weight); 


}

//*********************************************************
Value* doaGetWeightByBoneIndex_cf(Value** arg_list, int count) {

	check_arg_count(doaGetWeightByBoneIndex, 3, count);

	INode* mesh = arg_list[0]->to_node();
	int bonenum = arg_list[1]->to_int();
	int vert = arg_list[2]->to_int();

	vert--;		// don't get all off-by-one coming out of MAXSCRIPT!
	bonenum--;

	Modifier* phy;
	float weight = 0.0;

	if (! (phy = FindPhysiqueModifier(mesh)) ) {
		// Could raise an exception?
		return &false_value;
	}

	IPhysiqueExport *phyExp = (IPhysiqueExport *)phy->GetInterface(I_PHYINTERFACE);

	if ( IPhyContextExport *phyContextExp = (IPhyContextExport *)phyExp->GetContextInterface(mesh) ) {

		phyContextExp->ConvertToRigid(true);
		phyContextExp->AllowBlending(true);

		int numverts = phyContextExp->GetNumberVertices();

		if (vert < 0 || numverts <= vert) {
			// This deserves an ERROR!
			phyExp->ReleaseContextInterface(phyContextExp);
			return &false_value;
		}


		if (IPhyVertexExport *phyVertExp = (IPhyVertexExport *)phyContextExp->GetVertexInterface(vert) ) {

			int vtype = phyVertExp->GetVertexType();

			if (vtype & BLENDED_TYPE) {

				IPhyBlendedRigidVertex* phyBlendVert = (IPhyBlendedRigidVertex *)phyVertExp;

				if ((bonenum >= 0) && (bonenum < phyBlendVert->GetNumberNodes())) {
					weight = phyBlendVert->GetWeight(bonenum);
				}

			} else {

				IPhyRigidVertex* phyRigidVert = (IPhyRigidVertex *)phyVertExp;
				if (bonenum == 0) {
					weight = 1.0;
				}

			}

			phyContextExp->ReleaseVertexInterface(phyVertExp);

		} else {
			// This deserves an ERROR!
			phyExp->ReleaseContextInterface(phyContextExp);
			return &false_value;
		}


		phyExp->ReleaseContextInterface(phyContextExp);

	}


	return Float::intern(weight); 


}

//*********************************************************

Value* doaGetBoneByVertexIndex_cf(Value** arg_list, int count) {

	check_arg_count(doaGetBoneByVertexIndex, 3, count);

	INode* mesh = arg_list[0]->to_node();
	int vert = arg_list[1]->to_int();
	int bonenum = arg_list[2]->to_int();

	vert--;		// don't get all off-by-one coming out of MAXSCRIPT!
	bonenum--;	

	Modifier* phy;

	INode* bone = NULL;

	if (! (phy = FindPhysiqueModifier(mesh)) ) {
		// Could raise an exception?
		return &false_value;
	}

	IPhysiqueExport *phyExp = (IPhysiqueExport *)phy->GetInterface(I_PHYINTERFACE);

	if ( IPhyContextExport *phyContextExp = (IPhyContextExport *)phyExp->GetContextInterface(mesh) ) {

		phyContextExp->ConvertToRigid(true);
		phyContextExp->AllowBlending(true);

		int numverts = phyContextExp->GetNumberVertices();

		if (vert < 0 || numverts <= vert) {
			// This deserves an ERROR!
			phyExp->ReleaseContextInterface(phyContextExp);
			return &false_value;
		}


		if (IPhyVertexExport *phyVertExp = (IPhyVertexExport *)phyContextExp->GetVertexInterface(vert) ) {

			int vtype = phyVertExp->GetVertexType();

			if (vtype & BLENDED_TYPE) {

				IPhyBlendedRigidVertex* phyBlendVert = (IPhyBlendedRigidVertex *)phyVertExp;

				bone = phyBlendVert->GetNode(bonenum);

			} else if (bonenum == 0 ){

				IPhyRigidVertex* phyRigidVert = (IPhyRigidVertex *)phyVertExp;

				bone = phyRigidVert->GetNode();

			}

			phyContextExp->ReleaseVertexInterface(phyVertExp);

		} else {
			// This deserves an ERROR!
			phyExp->ReleaseContextInterface(phyContextExp);
			return &false_value;
		}


		phyExp->ReleaseContextInterface(phyContextExp);

	}

	if (bone) {

		one_typed_value_local(MAXNode* result);
		vl.result = new MAXNode(bone);

		return_value(vl.result);

	}

	return &false_value; 


}

//*********************************************************

Value* doaGetTotalWeight_cf(Value** arg_list, int count) {

	check_arg_count(doaGetTotalWeight, 2, count);

	INode* mesh = arg_list[0]->to_node();
	int vert = arg_list[1]->to_int();

	vert--;		// don't get all off-by-one coming out of MAXSCRIPT!

	Modifier* phy;
	float totalweight = 0.0;

	if (! (phy = FindPhysiqueModifier(mesh)) ) {
		// Could raise an exception?
		return &false_value;
	}

	IPhysiqueExport *phyExp = (IPhysiqueExport *)phy->GetInterface(I_PHYINTERFACE);

	if ( IPhyContextExport *phyContextExp = (IPhyContextExport *)phyExp->GetContextInterface(mesh) ) {

		phyContextExp->ConvertToRigid(true);
		phyContextExp->AllowBlending(true);

		int numverts = phyContextExp->GetNumberVertices();

		if (vert < 0 || numverts <= vert) {
			// This deserves an ERROR!
			phyExp->ReleaseContextInterface(phyContextExp);
			return &false_value;
		}


		if (IPhyVertexExport *phyVertExp = (IPhyVertexExport *)phyContextExp->GetVertexInterface(vert) ) {

			int vtype = phyVertExp->GetVertexType();

			if (vtype & BLENDED_TYPE) {

				IPhyBlendedRigidVertex* phyBlendVert = (IPhyBlendedRigidVertex *)phyVertExp;

				for (int b = 0; b < phyBlendVert->GetNumberNodes(); b++) {
					totalweight += phyBlendVert->GetWeight(b);
				}

			} else {

				totalweight = -1.0;

			}

			phyContextExp->ReleaseVertexInterface(phyVertExp);

		} else {
			// This deserves an ERROR!
			phyExp->ReleaseContextInterface(phyContextExp);
			return &false_value;
		}


		phyExp->ReleaseContextInterface(phyContextExp);

	}


	return Float::intern(totalweight); 


}

//*********************************************************

Value* doaValidFootstep_cf(Value** arg_list, int count) {

	check_arg_count(doaValidFootstep, 1, count);

	INode* bone = arg_list[0]->to_node();

	if (bone) { 

		// Get the node's transform control
		Control *c = bone->GetTMController();

		if (c && (c->ClassID() == FOOTPRINT_CLASS_ID) ) {

			return &true_value;

		}
	}

	return &false_value;

}

//*********************************************************

Value* doaValidBody_cf(Value** arg_list, int count) {

	check_arg_count(doaValidBody, 1, count);

	INode* bone = arg_list[0]->to_node();

	if (bone) { 

		// Get the node's transform control
		Control *c = bone->GetTMController();

		if (c && (c->ClassID() == BIPBODY_CONTROL_CLASS_ID)) {

			return &true_value;

		}
	}

	return &false_value;

}
//*********************************************************

Value* doaValidBone_cf(Value** arg_list, int count) {

	check_arg_count(doaValidBone, 1, count);

	INode* bone = arg_list[0]->to_node();

	if (bone) { 

		// Get the node's transform control
		Control *c = bone->GetTMController();

		if (c && (c->ClassID() == BIPSLAVE_CONTROL_CLASS_ID)) {

			return &true_value;

		}
	}

	return &false_value;

}

//*********************************************************

Value* doaValidRigidVertex_cf(Value** arg_list, int count) {

	check_arg_count(doaValidRigidVertex, 2, count);

	INode* mesh = arg_list[0]->to_node();
	int vert = arg_list[1]->to_int();

	vert--;		// don't get all off-by-one coming out of MAXSCRIPT!

	Modifier* phy;
	bool IsRigid = false;

	if (! (phy = FindPhysiqueModifier(mesh)) ) {
		// Could raise an exception?
		return &false_value;
	}

	IPhysiqueExport *phyExp = (IPhysiqueExport *)phy->GetInterface(I_PHYINTERFACE);

	if ( IPhyContextExport *phyContextExp = (IPhyContextExport *)phyExp->GetContextInterface(mesh) ) {

		phyContextExp->ConvertToRigid(false);

		int numverts = phyContextExp->GetNumberVertices();

		if (vert >= 0 && numverts > vert) {

			IPhyVertexExport *phyVertExp = (IPhyVertexExport *)phyContextExp->GetVertexInterface(vert);

			if (phyVertExp) {
				phyContextExp->ReleaseVertexInterface(phyVertExp);
				IsRigid = true;
			} 
		}

		phyExp->ReleaseContextInterface(phyContextExp);

	}


	if (IsRigid) {
		return &true_value;
	}

	return &false_value;
}

//*********************************************************

Value* doaNumTMKeys_cf(Value** arg_list, int count) {
	check_arg_count(doaNumKeys, 1, count);

	INode* bone = arg_list[0]->to_node();

	int val = 0;

	if (bone) { 

		// Get the node's transform control
		Control *c = bone->GetTMController();

		if (c && (c->ClassID() == BIPSLAVE_CONTROL_CLASS_ID)) {

			val = c->NumKeys();

		}

	}

	return Integer::intern(val);
}

//*********************************************************

Value* doaNumVerticalKeys_cf(Value** arg_list, int count) {
	check_arg_count(doaNumVerticalKeys, 1, count);

	INode* bone = arg_list[0]->to_node();

	int val = 0;

	if (bone) { 

		// Get the node's transform control
		Control *c = bone->GetTMController();

		if (c && (c->ClassID() == BIPBODY_CONTROL_CLASS_ID)) {

			val = c->SubAnim(0)->NumKeys();

		}

	}

	return Integer::intern(val);
}
//*********************************************************

Value* doaNumHorizontalKeys_cf(Value** arg_list, int count) {
	check_arg_count(doaNumHorizontalKeys, 1, count);

	INode* bone = arg_list[0]->to_node();

	int val = 0;

	if (bone) { 

		// Get the node's transform control
		Control *c = bone->GetTMController();

		if (c && (c->ClassID() == BIPBODY_CONTROL_CLASS_ID)) {

			val = c->SubAnim(1)->NumKeys();

		}

	}

	return Integer::intern(val);
}
//*********************************************************

Value* doaNumRotationKeys_cf(Value** arg_list, int count) {
	check_arg_count(doaNumRotationKeys, 1, count);

	INode* bone = arg_list[0]->to_node();

	int val = 0;

	if (bone) { 

		// Get the node's transform control
		Control *c = bone->GetTMController();

		if (c && (c->ClassID() == BIPBODY_CONTROL_CLASS_ID)) {

			val = c->SubAnim(2)->NumKeys();

		}

	}

	return Integer::intern(val);
}

//*********************************************************

Value* doaGetTMKeyTimes_cf(Value** arg_list, int count) {
	check_arg_count(doaGetKeyTimes, 1, count);

	INode* bone = arg_list[0]->to_node();

	int offset = 0;

	one_typed_value_local(Array* result);

	vl.result = new Array(100); 
	
	if (bone) { 

		// Get the node's transform control
		Control *c = bone->GetTMController();

		if (c && (c->ClassID() == BIPSLAVE_CONTROL_CLASS_ID)) {

			Tab<TimeValue> timetable;

			offset = c->GetKeyTimes(timetable,Interval(GetAnimStart(),GetAnimEnd()),0);

			{for (int k=0; k < timetable.Count(); k++ ){
				vl.result->append(MSTime::intern(timetable[k]));
			}}

		}

	}

	return_value(vl.result);
}

//*********************************************************

Value* doaGetHorizontalKeyTimes_cf(Value** arg_list, int count) {
	check_arg_count(doaGetHorizontalKeyTimes, 1, count);

	INode* bone = arg_list[0]->to_node();

	int offset = 0;

	one_typed_value_local(Array* result);

	vl.result = new Array(100); 
	
	if (bone) { 

		// Get the node's transform control
		Control *c = bone->GetTMController();

		if (c &&  (c->ClassID() == BIPBODY_CONTROL_CLASS_ID)) {

			Tab<TimeValue> timetable;

			offset = c->SubAnim(0)->GetKeyTimes(timetable,Interval(GetAnimStart(),GetAnimEnd()),0);

			{for (int k=0; k < timetable.Count(); k++ ){
				vl.result->append(MSTime::intern(timetable[k]));
			}}

		}

	}

	return_value(vl.result);
}


//*********************************************************
Value* doaGetVerticalKeyTimes_cf(Value** arg_list, int count) {
	check_arg_count(doaGetVerticalKeyTimes, 1, count);

	INode* bone = arg_list[0]->to_node();

	int offset = 0;

	one_typed_value_local(Array* result);

	vl.result = new Array(100); 
	
	if (bone) { 

		// Get the node's transform control
		Control *c = bone->GetTMController();

		if (c &&  (c->ClassID() == BIPBODY_CONTROL_CLASS_ID)) {

			Tab<TimeValue> timetable;

			offset = c->SubAnim(1)->GetKeyTimes(timetable,Interval(GetAnimStart(),GetAnimEnd()),0);

			{for (int k=0; k < timetable.Count(); k++ ){
				vl.result->append(MSTime::intern(timetable[k]));
			}}

		}

	}

	return_value(vl.result);
}


//*********************************************************
Value* doaGetRotationKeyTimes_cf(Value** arg_list, int count) {
	check_arg_count(doaGetRotationKeyTimes, 1, count);

	INode* bone = arg_list[0]->to_node();

	int offset = 0;

	one_typed_value_local(Array* result);

	vl.result = new Array(100); 
	
	if (bone) { 

		// Get the node's transform control

		Control *c = bone->GetTMController();

		if (c &&  c->ClassID() == BIPBODY_CONTROL_CLASS_ID) {

			Tab<TimeValue> timetable;

			offset = c->SubAnim(2)->GetKeyTimes(timetable,Interval(GetAnimStart(),GetAnimEnd()),0);

			{for (int k=0; k < timetable.Count(); k++ ){
				vl.result->append(MSTime::intern(timetable[k]));
			}}

		}

	}

	return_value(vl.result);
}

//*********************************************************
Value* doaGetNextKeyTime_cf(Value** arg_list, int count) {

	check_arg_count(doaGetNextKeyTime, 2, count);

	INode* bone = arg_list[0]->to_node();
	TimeValue prevtime = arg_list[1]->to_timevalue();


	TimeValue t;

	Control *c = bone->GetTMController();

	DWORD flags = NEXTKEY_RIGHT;//|NEXTKEY_POS|NEXTKEY_ROT|NEXTKEY_SCALE;

	TimeValue next_time = TIME_PosInfinity;
	
	if (c) {

		if (c->ClassID() == BIPBODY_CONTROL_CLASS_ID) {

			{for (int i = 0; i < 3; i ++) {
				if (c->SubAnim(i)->GetNextKeyTime(prevtime,flags,t)) {
					if (t < next_time) {
						next_time = t;
					} 
				}
			}}


		} else if (c->GetNextKeyTime(prevtime,flags,t)) {

			next_time = t;

		}
	}

	if (next_time != TIME_PosInfinity) {
		return MSTime::intern(next_time);
	}

	return &false_value;

}

//*********************************************************
Value* doaSetKeyTMValue_cf(Value** arg_list, int count) {

	check_arg_count(doaSetKeyTMValue, 3, count);

	INode* bone = arg_list[0]->to_node();
	TimeValue t = arg_list[1]->to_timevalue();
	Matrix3 relativetm = arg_list[2]->to_matrix3();

	DWORD flags = NEXTKEY_RIGHT; //|NEXTKEY_POS|NEXTKEY_ROT|NEXTKEY_SCALE;

	Control *c = bone->GetTMController();

	if (c) {
		c->SetValue(t, &SetXFormPacket(relativetm), 1,CTRL_RELATIVE);
		return &true_value;
	}

	return &false_value;

}

#include "decomp.h"

//*********************************************************
Matrix3 MakeUniform(Matrix3& non_uniform) {

	AffineParts parts;
	Matrix3 uniform;

	decomp_affine(non_uniform,&parts);

	parts.q.MakeMatrix(uniform);

	uniform.SetRow(3,parts.t);

	return uniform;

}

//*********************************************************

Value* doaUniformMatrix_cf(Value** arg_list, int count) {

	check_arg_count(doaUniformMatrix,1, count);

	Matrix3& orig = arg_list[0]->to_matrix3();

	Matrix3 uniform = MakeUniform(orig);

	one_typed_value_local(Matrix3Value* result);

	vl.result = new Matrix3Value(uniform);

	return_value(vl.result);
      
}

//*********************************************************

Value* doaRelativeMatrix_cf(Value** arg_list, int count) {

	check_arg_count(doaRelativeMatrix,2, count);

	INode* bone = arg_list[0]->to_node();

	TimeValue t = arg_list[1]->to_timevalue();

    
	/* Note: This function removes the non-uniform scaling 
	from MAX node transformations. before multiplying the 
	current node by  the inverse of its parent. The 
	removal  must be done on both nodes before the 
	multiplication and Inverse are applied. This is especially 
	useful for Biped export (which uses non-uniform scaling on 
	its body parts.) */

	Matrix3  cur_mat;       // for current and parent 
	Matrix3  par_mat;       // decomposed matrices 
                                                                 
	//Get transformation matrices

	cur_mat = MakeUniform(bone->GetNodeTM(t));

	INode *p_node = bone->GetParentNode();

	if (p_node) {
		par_mat = MakeUniform(p_node->GetNodeTM(t));
	} else {
		par_mat = Matrix3(1); 
	}

	one_typed_value_local(Matrix3Value* result);
	vl.result = new Matrix3Value(cur_mat * Inverse( par_mat));

	return_value(vl.result);
      
}

//*********************************************************

Value* doaAbsoluteMatrix_cf(Value** arg_list, int count) {

	check_arg_count(doaAbsoluteMatrix,2, count);

	INode* bone = arg_list[0]->to_node();

	TimeValue t = arg_list[1]->to_timevalue();

	Matrix3  cur_mat = MakeUniform(bone->GetNodeTM(t));

	one_typed_value_local(Matrix3Value* result);
	vl.result = new Matrix3Value(cur_mat);

	return_value(vl.result);
      
}


//*********************************************************

Value* doaStringToInt_cf(Value** arg_list, int count) {

	check_arg_count(doaStringToInt,2, count);

	char* numstring = arg_list[0]->to_string();
	char* fmt = arg_list[1]->to_string();

	int val;
	sscanf(numstring,fmt, &val);

	return Integer::intern(val);
  
    
}

//*********************************************************

Value* doaIntToName_cf(Value** arg_list, int count) {

	check_arg_count(doaIntToName,2, count);

	int val = arg_list[0]->to_int();
	char* fmt = arg_list[1]->to_string();

	char buffer[16];
	sprintf(buffer, fmt, val);


	return Name::intern(buffer);
    
}
