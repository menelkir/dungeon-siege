//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by FontScript.rc
//
#define IDD_FONTEXPORT                  101
#define IDI_ICON1                       102
#define IDC_FONT_NAMES                  1000
#define IDC_FONT_SIZE                   1002
#define IDC_FONTSIZE                    1003
#define IDC_BOLD                        1004
#define IDC_ITALIC                      1005
#define IDC_UNDERLINE                   1006
#define IDC_SAVE_FONT                   1007
#define IDC_ASCII_LOW                   1008
#define IDC_ASCII_HIGH                  1009
#define IDC_RESIZE                      1013
#define IDC_SELECT_BACKGROUND_COLOR     1014
#define IDC_SELECT_TEXT_COLOR           1015
#define IDC_WIDTH                       1016
#define IDC_HEIGHT                      1017
#define IDC_SPACING                     1018
#define IDC_CHECK_SMOOTHING             1019

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        103
#define _APS_NEXT_COMMAND_VALUE         40001
#define _APS_NEXT_CONTROL_VALUE         1021
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
