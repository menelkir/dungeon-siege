Attribute VB_Name = "modDeclares"
Option Explicit

' Delimiter for the logs generated by DS report streams
Global Const sHeadDelim$ = "-==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==-"
Global Const sReservedChar = "!@#$%^&*()-+|\/<>,.~`{};:[]?="

Enum CaseType
    Spell = 5
    UnitTest = 1
    pcontent = 4
    FPS = 7
End Enum

Global CurrentCaseType As CaseType

Global Const DEBUG_EXE = "DungeonSiegeD.exe"
Global Const RELEASE_EXE = "DungeonSiegeR.exe"
Global Const PCONTENT_SKRIT = "pcontent_gen"
Global Const FPS_SKRIT = "FpsTest"

Public TimeDate As String, tLaunchTD As String * 23 ' timedate as of lauch of Dungeon Siege


'Case Action Vars

Enum CaseAction
    TC_EDIT_CASE = 1
    TC_NEW_CASE = 2
End Enum

Global eCaseAction As CaseAction


Enum PathType
    PT_SKRIT_PATH = 1
    PT_EXE_PATH = 2
End Enum

'Variable to store the command line string
Type PContentinfo
    PContentQuery As String
    PcontentExcelFileName As String
    PcontentCaseName As String
    PcontentIterations As Integer
End Type

Global CurrentPcontent As PContentinfo

Global sCommandLineString As String


Global bIsNameOk As Boolean
Global bIsDBPathOk As Boolean
Global bIsSkritPathOk As Boolean
Global iCaseCount As Integer

'User Structure
Type UserInfo
    SQLserver As String
    UserName As String
    ExePath As String
    SkritPath As String
    IsDebug As Boolean
    MachineName As String
    OS As String
    MinimizeMain As Boolean
    MinimizeLog As Boolean
    CanWrite As Boolean
    CurrentBuild As String
    
End Type

Type Spells
    SpellName As String
    MonsterName As String
    PassFail As Boolean
    MachineName As String
    BuildNumber As String
    TimeDateStamp As String
    LogData As String
End Type
Global SpellCaseArray() As Spells

'Global userinfo struct for current user
Global CurrentUserInfo As UserInfo

'Log Structure
Public Type LogInfo
    TimeDateStamp As String
    CaseName As String  'THis is optional if the case is a spell or pcontent
    PcontentQueryString As String
    MachineName As String
    Pass As Boolean
    LogData As String
    UserName As String
    IsDebug As Boolean
    BuildNumber As String
End Type

'Case structure
Type Caseinfo
    TestArea As String      'This is the field in the db known as 'Area'
    CaseName As String      'This is the field 'Name' in the db
    Map As String
    Skrit As String
    Arguments As String
    Description As String
    Expected As String
    Teleport As String      'The book mark on the map where the character starts
    Author As String        'This will be the actual Author name of whomever authored the case
    index As Integer        'This will actually be used to store the index of the case's node in the tree
    CaseNum As Integer      'This is the id that is stored in the db
    SkritPath As String
End Type

Type FPSInfo
    TeleportName As String
    TestDone As Boolean
    OpenExcel As Boolean
    ExcelWorksheetName As String
    CurrentExcelFileName As String
    IsMapWorld As Boolean
    SampleDuration As Integer
End Type

Global CurrentFPSInfo As FPSInfo


'Array to hold the case structures
Public CaseArray() As Caseinfo
