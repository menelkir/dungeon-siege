/**************************************************************************

Filename		: Main.h

Description		: Function prototypes/defines for the entry point file

Creation Date	: 6/30/99

Author			: Chad Queen

**************************************************************************/

#pragma once 
#ifndef _MAIN_H_
#define _MAIN_H_

// Include Files
#include "Shell.h"

// Toolbar Defines
#define TOOLBARHEIGHT				26

#endif