//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by Network_transport_test.rc
//
#define IDD_NETWORK_TRANSPORT_TEST_DIALOG 102
#define IDR_MAINFRAME                   128
#define IDEXIT                          1000
#define IDC_SESSIONSCOMBO               1003
#define IDC_LISTSESSIONS                1004
#define IDC_JOIN                        1005
#define IDC_CONNECTIONSCOMBO            1005
#define IDC_CREATESESSION               1006
#define IDC_CREATEANDOPENSESSION        1006
#define IDC_CREATECONNECTION            1007
#define IDC_LIST2                       1008
#define IDC_LISTCONNECTIONS             1009
#define IDC_NEWSESSIONNAME              1010
#define IDC_NEWCONNECTIONNAME           1011
#define IDC_LISTPROVIDERS               1012
#define IDC_PROVIDERLIST                1013
#define IDC_OPENSESSION                 1014
#define IDC_CLOSECONNECTION             1015
#define IDC_INITPROVIDER                1016
#define IDC_RESETNETWORKTRANSPORT       1017

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        129
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1018
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
