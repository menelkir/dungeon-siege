/**************************************************************************

Filename		: Main.h

Description		: Function prototypes/defines for the entry point file

Creation Date	: 4/29/99

Author			: Chad Queen

**************************************************************************/


#ifndef _MAIN_H_
#define _MAIN_H_

// Include Files
#include <windows.h>

// Function Prototypes
int WINAPI HandledWinMain( HINSTANCE hInstance, HINSTANCE hPrevInstance, PSTR szCmdLine, int iCmdShow );

#endif