//////////////////////////////////////////////////////////////////////////////
//
// File     :  Precomp_UITest.h
// Author(s):  Scott Bilas
//
// Summary  :  Contains the headers that will be precompiled for this project.
//
// Copyright � 1999 Gas Powered Games, Inc.  All rights reserved.
//----------------------------------------------------------------------------
//  $Revision:: $              $Date:$
//----------------------------------------------------------------------------
//////////////////////////////////////////////////////////////////////////////

#pragma once
#ifndef __PRECOMP_UITEST_H
#define __PRECOMP_UITEST_H

//////////////////////////////////////////////////////////////////////////////

#include "gpcore.h"

//////////////////////////////////////////////////////////////////////////////

#endif  // __PRECOMP_UITEST_H

//////////////////////////////////////////////////////////////////////////////
