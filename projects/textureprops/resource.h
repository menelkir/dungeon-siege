//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by TexturePropsRes.rc
//
#define IDD_TEXTUREPROPS                101
#define IDC_HOMEPATH                    1000
#define IDC_FILENAME                    1001
#define IDC_IMPORT                      1002
#define IDC_EXPORT                      1003
#define IDC_FUELROOT                    1004
#define IDC_TSDVALS                     1005
#define IDC_PROGRESS                    1007
#define IDC_STATUS                      1008
#define IDC_CLOSE                       1009
#define IDC_TEXTUREDIMS                 1010
#define IDC_OPENEXCEL                   1012

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        102
#define _APS_NEXT_COMMAND_VALUE         40001
#define _APS_NEXT_CONTROL_VALUE         1013
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
