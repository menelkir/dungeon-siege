//////////////////////////////////////////////////////////////////////////////
//
// File     :  PrecompFuBiPacker.h
// Author(s):  Scott Bilas
//
// Summary  :  Contains the headers that will be precompiled for this project.
//
// Copyright � 2000 Gas Powered Games, Inc.  All rights reserved.
//----------------------------------------------------------------------------
//  $Revision:: $              $Date:$
//----------------------------------------------------------------------------
//////////////////////////////////////////////////////////////////////////////

#pragma once
#ifndef __PRECOMPFUBIPACKER_H
#define __PRECOMPFUBIPACKER_H

//////////////////////////////////////////////////////////////////////////////

#include "GpCore.h"

//////////////////////////////////////////////////////////////////////////////

#endif  // __PRECOMPFUBIPACKER_H

//////////////////////////////////////////////////////////////////////////////
