#if !defined(AFX_DIALOG_CONVERSATION_LIST_GLOBAL_H__60027667_A8C1_49F7_A07F_A09613F46EA0__INCLUDED_)
#define AFX_DIALOG_CONVERSATION_LIST_GLOBAL_H__60027667_A8C1_49F7_A07F_A09613F46EA0__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// Dialog_Conversation_List_Global.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// Dialog_Conversation_List_Global dialog

class Dialog_Conversation_List_Global : public CDialog, public Singleton <Dialog_Conversation_List_Global>
{
// Construction
public:
	Dialog_Conversation_List_Global(CWnd* pParent = NULL);   // standard constructor

	void SetSelectedConversation( gpstring sConversation )	{ m_sSelectedConversation = sConversation; }
	gpstring GetSelectedConversation()						{ return m_sSelectedConversation; }

// Dialog Data
	//{{AFX_DATA(Dialog_Conversation_List_Global)
	enum { IDD = IDD_DIALOG_CONVERSATION_LIST_GLOBAL };
	CListBox	m_Conversations;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(Dialog_Conversation_List_Global)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	void Refresh();

	gpstring m_sSelectedConversation;

	// Generated message map functions
	//{{AFX_MSG(Dialog_Conversation_List_Global)
	virtual BOOL OnInitDialog();
	afx_msg void OnButtonHelp();
	afx_msg void OnButtonNew();
	afx_msg void OnButtonEdit();
	afx_msg void OnButtonRemove();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

#define gConversationListGlobal Singleton <Dialog_Conversation_List_Global>::GetSingleton()

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DIALOG_CONVERSATION_LIST_GLOBAL_H__60027667_A8C1_49F7_A07F_A09613F46EA0__INCLUDED_)
