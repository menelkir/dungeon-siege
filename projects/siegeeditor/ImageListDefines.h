//////////////////////////////////////////////////////////////////////////////
//
// File     :  ImageListDefines.h
// Author(s):  Chad Queen
//
// Copyright � 2000 Gas Powered Games, Inc.  All rights reserved.
//----------------------------------------------------------------------------
//  $Revision:: $              $Date:$
//----------------------------------------------------------------------------
//////////////////////////////////////////////////////////////////////////////


#pragma once
#ifndef __IMAGELISTDEFINES_H

#define CX_ICON				16
#define CY_ICON				16
#define NUM_ICONS			13

#define IMAGE_GO			0
#define IMAGE_SN			1
#define IMAGE_REGION		2
#define IMAGE_3BOX			3
#define IMAGE_LTBLUEBOX		4
#define IMAGE_CLOSEFOLDER	5
#define IMAGE_OPENFOLDER	6
#define IMAGE_REDO			7
#define IMAGE_UNDO			8
#define IMAGE_LIGHT			9
#define IMAGE_HOTPOINT		10
#define IMAGE_DECAL			11
#define IMAGE_STARTINGPOS	12

#endif
