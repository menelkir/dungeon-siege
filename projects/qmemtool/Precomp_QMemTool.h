//////////////////////////////////////////////////////////////////////////////
//
// File     :  Precomp_QMemTool.h
// Author(s):  Scott Bilas
//
// Summary  :  Contains the headers that will be precompiled for this project.
//
// Copyright � 2001 Gas Powered Games, Inc.  All rights reserved.
//----------------------------------------------------------------------------
//  $Revision:: $              $Date:$
//----------------------------------------------------------------------------
//////////////////////////////////////////////////////////////////////////////

#pragma once
#ifndef __PRECOMP_QMEMTOOL_H
#define __PRECOMP_QMEMTOOL_H

//////////////////////////////////////////////////////////////////////////////

#include "Precomp_GpCore.h"

#include "AppModule.h"
#include "AtlX.h"
#include "Config.h"

//////////////////////////////////////////////////////////////////////////////

#endif  // __PRECOMP_QMEMTOOL_H

//////////////////////////////////////////////////////////////////////////////
