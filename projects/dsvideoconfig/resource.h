//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by DSVideoConfig.rc
//
#define IDS_STRING_HARDWARE             1
#define IDS_STRING_HARDWARE_TNL         2
#define IDS_STRING_UNKNOWN              3
#define IDS_STRING_COMPLEX_SHADOWS      4
#define IDS_STRING_SIMPLE_SHADOWS       5
#define IDS_STRING_NO_SHADOWS           6
#define IDS_STRING_PARTY_SHADOWS        7
#define IDS_STRING_BILINEAR_FILTERING   8
#define IDS_STRING_TRILINEAR_FILTERING  9
#define IDS_STRING_BELOW_MIN_SPEC_WARN  10
#define IDS_STRING_HARDWARE_WARNING     11
#define IDD_DIALOG1                     101
#define IDD_DIALOG_VIDEO_CONFIG         101
#define IDI_ICON                        102
#define IDC_COMBO_DRIVER                1000
#define IDC_COMBO_RESOLUTION            1001
#define IDC_CHECK_BLTONLY               1002
#define IDC_BUTTON_TEST                 1003
#define IDC_HARDWARENAME                1004
#define IDC_SHADOWGROUP                 1007
#define IDC_SHADOWCOMBO                 1008
#define IDC_FILTERGROUP                 1009
#define IDC_FILTERCOMBO                 1010

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        103
#define _APS_NEXT_COMMAND_VALUE         40001
#define _APS_NEXT_CONTROL_VALUE         1009
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
