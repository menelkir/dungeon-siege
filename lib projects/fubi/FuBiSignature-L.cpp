//////////////////////////////////////////////////////////////////////////////
//
// File     :  FuBiSignature-L.cpp
// Author(s):  Scott Bilas
//
// Copyright � 2002 Gas Powered Games, Inc.  All rights reserved.
//----------------------------------------------------------------------------
//  $Revision:: $              $Date:$
//----------------------------------------------------------------------------
//////////////////////////////////////////////////////////////////////////////

#include "Precomp_FuBi.h"

#if GP_DEBUG
#include "FuBiSignature-L-Debug.cpp.out"
#elif GP_RELEASE
#include "FuBiSignature-L-Release.cpp.out"
#elif GP_RETAIL
#include "FuBiSignature-L-Retail.cpp.out"
#elif GP_PROFILING
#include "FuBiSignature-L-Profiling.cpp.out"
#endif

//////////////////////////////////////////////////////////////////////////////
