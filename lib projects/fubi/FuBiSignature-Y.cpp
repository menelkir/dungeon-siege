//////////////////////////////////////////////////////////////////////////////
//
// File     :  FuBiSignature-Y.cpp
// Author(s):  Scott Bilas
//
// Copyright � 2002 Gas Powered Games, Inc.  All rights reserved.
//----------------------------------------------------------------------------
//  $Revision:: $              $Date:$
//----------------------------------------------------------------------------
//////////////////////////////////////////////////////////////////////////////

#include "Precomp_FuBi.h"

#if GP_DEBUG
#include "FuBiSignature-Y-Debug.cpp.out"
#elif GP_RELEASE
#include "FuBiSignature-Y-Release.cpp.out"
#elif GP_RETAIL
#include "FuBiSignature-Y-Retail.cpp.out"
#elif GP_PROFILING
#include "FuBiSignature-Y-Profiling.cpp.out"
#endif

//////////////////////////////////////////////////////////////////////////////
