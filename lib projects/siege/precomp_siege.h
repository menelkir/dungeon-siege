//////////////////////////////////////////////////////////////////////////////
//
// File     :  precomp_siege.h
// Author(s):  Scott Bilas
//
// Summary  :  Contains the headers that will be precompiled for this project.
//
// Copyright � 1999 Gas Powered Games, Inc.  All rights reserved.
//----------------------------------------------------------------------------
//  $Revision:: $              $Date:$
//----------------------------------------------------------------------------
//////////////////////////////////////////////////////////////////////////////

#pragma once
#ifndef __PRECOMP_SIEGE_H
#define __PRECOMP_SIEGE_H

//////////////////////////////////////////////////////////////////////////////

#include "gpcore.h"

#include "siege_defs.h"

#include "siege_cache.h"
#include "siege_cache_handle.h"
#include "siege_camera.h"
#include "siege_compass.h"
#include "siege_database.h"
#include "siege_database_guid.h"
#include "siege_engine.h"
#include "siege_hotpoint_database.h"
#include "siege_logical_mesh.h"
#include "siege_logical_node.h"
#include "siege_mesh.h"
#include "siege_mesh_door.h"
#include "siege_mesh_io.h"
#include "siege_mouse_shadow.h"
#include "siege_node.h"
#include "siege_node_io.h"
#include "siege_options.h"
#include "siege_pos.h"
#include "siege_viewfrustum.h"
#include "siege_walker.h"

#include "matrix_3x3.h"
#include "filter_1.h"
#include "vector_3.h"
#include "triangle_3.h"
#include "space_3.h"

#include "RapiOwner.h"


//////////////////////////////////////////////////////////////////////////////

#endif  // __PRECOMP_SIEGE_H

//////////////////////////////////////////////////////////////////////////////
