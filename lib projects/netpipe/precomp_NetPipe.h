//////////////////////////////////////////////////////////////////////////////
//
// File     :  precomp_NetPipe.h
// Author(s):  Scott Bilas
//
// Summary  :  Contains the headers that will be precompiled for this project.
//
// Copyright � 1999 Gas Powered Games, Inc.  All rights reserved.
//----------------------------------------------------------------------------
//  $Revision:: $              $Date:$
//----------------------------------------------------------------------------
//////////////////////////////////////////////////////////////////////////////

#pragma once
#ifndef __PRECOMP_NETPIPE_H
#define __PRECOMP_NETPIPE_H

//////////////////////////////////////////////////////////////////////////////

#include "gpcore.h"

#pragma warning ( push, 1 )		// ignore any warnings or changes that win32 has
//#include <dxerr8.h>
//#include <dplay8.h>
//#include <dplobby8.h>
#pragma warning ( pop )			// back to normal

//////////////////////////////////////////////////////////////////////////////

#endif  // __PRECOMP_NetPipe_H

//////////////////////////////////////////////////////////////////////////////
