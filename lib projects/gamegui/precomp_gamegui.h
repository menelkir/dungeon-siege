//////////////////////////////////////////////////////////////////////////////
//
// File     :  precomp_gamegui.h
// Author(s):  Scott Bilas
//
// Summary  :  Contains the headers that will be precompiled for this project.
//
// Copyright � 1999 Gas Powered Games, Inc.  All rights reserved.
//----------------------------------------------------------------------------
//  $Revision:: $              $Date:$
//----------------------------------------------------------------------------
//////////////////////////////////////////////////////////////////////////////

#pragma once
#ifndef __PRECOMP_GAMEGUI_H
#define __PRECOMP_GAMEGUI_H

//////////////////////////////////////////////////////////////////////////////

#include "gpcore.h"

// from libs
#include "fuel.h"
#include "rapiowner.h"
#include "stringtool.h"

// from game gui
#include "ui_window.h"
#include "ui_listbox.h"
#include "ui_messenger.h"
#include "ui_textureman.h"
#include "ui_slider.h"
#include "ui_animation.h"

//////////////////////////////////////////////////////////////////////////////

#endif  // __PRECOMP_GAMEGUI_H

//////////////////////////////////////////////////////////////////////////////
