//////////////////////////////////////////////////////////////////////////////
//
// File     :  precomp_gamegui.cpp
// Author(s):  Scott Bilas
//
// Copyright � 1999 Gas Powered Games, Inc.  All rights reserved.
//----------------------------------------------------------------------------
//  $Revision:: $              $Date:$
//----------------------------------------------------------------------------
//////////////////////////////////////////////////////////////////////////////

#include "precomp_gamegui.h"

//////////////////////////////////////////////////////////////////////////////

/*
  This file contains all the symbols for the precompiled header. To use in
  a project, set the project to "Use Precompiled Headers" up to
  precomp_<project>.h, and then set this file "Create Precompiled Headers" up
  to precomp_<project>.h.

  To modify: leave precomp_<project>.cpp alone. It should only #include
  precomp_<project>.h. Instead, modify precomp_<project>.h on a per-project
  basis to tune the precompiled header and speed up builds. Add #include files
  that change rarely and are used widely in the project.
*/

//////////////////////////////////////////////////////////////////////////////
