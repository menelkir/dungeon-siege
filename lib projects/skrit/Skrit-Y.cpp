//////////////////////////////////////////////////////////////////////////////
//
// File     :  Skrit-Y.cpp
// Author(s):  Scott Bilas
//
// Copyright � 2002 Gas Powered Games, Inc.  All rights reserved.
//----------------------------------------------------------------------------
//  $Revision:: $              $Date:$
//----------------------------------------------------------------------------
//////////////////////////////////////////////////////////////////////////////

#include "Precomp_Skrit.h"

#if GP_DEBUG
#include "Skrit-Y-Debug.cpp.out"
#elif GP_RELEASE
#include "Skrit-Y-Release.cpp.out"
#elif GP_RETAIL
#include "Skrit-Y-Retail.cpp.out"
#elif GP_PROFILING
#include "Skrit-Y-Profiling.cpp.out"
#endif

//////////////////////////////////////////////////////////////////////////////
