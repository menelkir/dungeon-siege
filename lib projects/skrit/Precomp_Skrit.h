//////////////////////////////////////////////////////////////////////////////
//
// File     :  Precomp_Skrit.h
// Author(s):  Scott Bilas
//
// Summary  :  Contains the headers that will be precompiled for this project.
//
// Copyright � 2000 Gas Powered Games, Inc.  All rights reserved.
//----------------------------------------------------------------------------
//  $Revision:: $              $Date:$
//----------------------------------------------------------------------------
//////////////////////////////////////////////////////////////////////////////

#pragma once
#ifndef __PRECOMP_SKRIT_H
#define __PRECOMP_SKRIT_H

//////////////////////////////////////////////////////////////////////////////

#include "FuBi.h"
#include "GpColl.h"
#include "ResHandle.h"
#include "SkritByteCode.h"
#include "SkritObject.h"
#include "SkritStructure.h"
#include "StdHelp.h"

#include <vector>
#include <map>
#include <list>

//////////////////////////////////////////////////////////////////////////////

#endif  // __PRECOMP_SKRIT_H

//////////////////////////////////////////////////////////////////////////////
