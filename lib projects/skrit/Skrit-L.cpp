//////////////////////////////////////////////////////////////////////////////
//
// File     :  Skrit-L.cpp
// Author(s):  Scott Bilas
//
// Copyright � 2002 Gas Powered Games, Inc.  All rights reserved.
//----------------------------------------------------------------------------
//  $Revision:: $              $Date:$
//----------------------------------------------------------------------------
//////////////////////////////////////////////////////////////////////////////

#include "Precomp_Skrit.h"

#if GP_DEBUG
#include "Skrit-L-Debug.cpp.out"
#elif GP_RELEASE
#include "Skrit-L-Release.cpp.out"
#elif GP_RETAIL
#include "Skrit-L-Retail.cpp.out"
#elif GP_PROFILING
#include "Skrit-L-Profiling.cpp.out"
#endif

//////////////////////////////////////////////////////////////////////////////
