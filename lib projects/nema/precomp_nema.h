//////////////////////////////////////////////////////////////////////////////
//
// File     :  precomp_rapi.h
// Author(s):  Scott Bilas
//
// Summary  :  Contains the headers that will be precompiled for this project.
//
// Copyright � 1999 Gas Powered Games, Inc.  All rights reserved.
//----------------------------------------------------------------------------
//  $Revision:: $              $Date:$
//----------------------------------------------------------------------------
//////////////////////////////////////////////////////////////////////////////

#pragma once
#ifndef __PRECOMP_NEMA_H
#define __PRECOMP_NEMAS_H

//////////////////////////////////////////////////////////////////////////////

#include "gpcore.h"

#include "FuBiDefs.h"
#include "SkritDefs.h"

#include "nema_aspect.h"
#include "nema_aspectmgr.h"
#include "nema_kevents.h"
#include "nema_prskeys.h"
#include "nema_keymgr.h"
#include "nema_skritsupport.h"
#include "nema_chore.h"

#include <list>
#include <memory.h>



#include "quat.h"
#include "rapiowner.h"
#include "space_3.h"		// Needed only for XRotationColumns!
#include "vector_3.h"

#include "BlindCallback.h"	// TODO:: Callbacks maintained in Choreographer? -- biddle


//////////////////////////////////////////////////////////////////////////////

#endif  // __PRECOMP_RAPI_H

//////////////////////////////////////////////////////////////////////////////
