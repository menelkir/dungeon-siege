//////////////////////////////////////////////////////////////////////////////
//
// File     :  precomp_flamethrower.h
// Author(s):  Scott Bilas
//
// Summary  :  Contains the headers that will be precompiled for this project.
//
// Copyright � 1999 Gas Powered Games, Inc.  All rights reserved.
//----------------------------------------------------------------------------
//  $Revision:: $              $Date:$
//----------------------------------------------------------------------------
//////////////////////////////////////////////////////////////////////////////

#pragma once
#ifndef __PRECOMP_FLAMETHROWER_H
#define __PRECOMP_FLAMETHROWER_H

//////////////////////////////////////////////////////////////////////////////

// from libs
#include "gpcore.h"
#include "BlindCallback.h"
#include "GPProfiler.h"
#include "fuel.h"

// from flamethrower
#include "WorldFx.h"
#include "flamethrower_tracker.h"
#include "flamethrower_interpreter.h"
#include "flamethrower_interpreter_macros.h"
#include "flamethrower_interpreter_commands.h"
#include "flamethrower_types.h"

// other
#include "gosupport.h"
#include "fubipersist.h"
#include "fubitraitsimpl.h"
#include "rapiowner.h"
#include "vector_3.h"
#include "stdhelp.h"
#include <list>
#include <map>
#include <deque>
#include <vector>

//////////////////////////////////////////////////////////////////////////////

#endif  // __PRECOMP_FLAMETHROWER_H

//////////////////////////////////////////////////////////////////////////////
