//////////////////////////////////////////////////////////////////////////////
//
// File     :  UserCore.h
// Author(s):  Scott Bilas
//
// Summary  :  Contains custom compiler directives or whatever. YOU decide.
//             This file is #included by GpCore.h and is meant to be modified
//             locally. Use this to tune your custom build on a global level.
//             For example, this is a great way to #if MY_NAME out a bunch of
//             code that only works on your machine.
//
// Copyright � 2000 Gas Powered Games, Inc.  All rights reserved.
//----------------------------------------------------------------------------
//  $Revision:: $              $Date:$
//----------------------------------------------------------------------------
//////////////////////////////////////////////////////////////////////////////

// #define SCOTT_BILAS 1    // change this to your name

//////////////////////////////////////////////////////////////////////////////
