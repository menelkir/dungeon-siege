# After the smoketest has deemed the current build to be adequate, this should be run.
#________________________________________________
#
#
# Before anything else happens, get rid of the files that were generated by the smoketest.
# All .ini and .log files, and prefs.gas need to be deleted.
system ("del P:\\vss_db_tattoo\\GPG\\Out\\*.ini /q /f");
system ("del P:\\vss_db_tattoo\\GPG\\Out\\*.log /q /f");
system ("del P:\\vss_db_tattoo\\GPG\\Out\\prefs.gas /f");

# Copy over the builds

print "\nCopying [Retail] to server...\n";
system ("cscript P:\\vss_db_tattoo\\GPG\\Batch\\BuildCopy_Retail.js");
print "\n";
system ("net send marshm \"Dungeon Siege [Retail] built and copied!\"");
system ("net send jmcmahon \"Dungeon Siege [Retail] built and copied!\"");

print "\nCopying [Release] to server...\n";
system ("cscript P:\\vss_db_tattoo\\GPG\\Batch\\BuildCopy_Release.js");
print "\n";
system ("net send marshm \"Dungeon Siege [Release] built and copied!\"");
system ("net send jmcmahon \"Dungeon Siege [Release] built and copied!\"");

print "\nCopying [Debug] to server...\n";
system ("cscript P:\\vss_db_tattoo\\GPG\\Batch\\BuildCopy_Debug.js");
print "\n";
system ("net send marshm \"Dungeon Siege [Debug] built and copied!\"");
system ("net send jmcmahon \"Dungeon Siege [Debug] built and copied!\"");

# Done copying builds


#MOVE contents of:
#
#P:\vss_db_tattoo\TATTOO_ASSETS\cdimage
#
#to
#
#P:\testing_data\current\CDimage_BITS_[fill in the version number]_DEV
#
# 
# version number can be acquired via:
# tankbuilder -nologo -getversion <exe path and name>
system ('p:\vss_db_tattoo\tattoo_assets\tools\tankbuilder -nologo -getversion P:\vss_db_tattoo\TATTOO_ASSETS\cdimage\dungeonsiege.exe > p:\currentversion.txt');
if (open (VERSION, "<" . 'p:\currentversion.txt')) {
	$line = <VERSION>;
	chomp $line;
	$line =~ /.*\.(\d*\.\d*\.\d*)/;
	$version = $1;
	close VERSION;
	system ('del p:\currentversion.txt');
}
else { print "Couldn't open p:\currentversion.txt!\n"; }
if (not $version) {$version = "unknown-update-this"; print "YOU WILL NEED TO MANUALLY SET THE VERSION NUMBER IN THE DIRECTORY NAMES\n";}
$bits_dir_name = "CDimage_BITS_" . $version . "_DEV";
system ('move P:\vss_db_tattoo\TATTOO_ASSETS\cdimage P:\testing_data\current');
system ('del P:\testing_data\current\cdimage\\*.scc /q /s /f'); #get rid of vss .scc files
system ('move P:\testing_data\current\cdimage P:\testing_data\current\\' . $bits_dir_name);
system ('attrib +r P:\testing_data\current\\' . $bits_dir_name . '\\* /s /d');
system ('mkdir P:\vss_db_tattoo\TATTOO_ASSETS\cdimage');
#________________________________________________
#
#MOVE contents of:
#
#P:\vss_db_tattoo\GPG\Out
#
#to
#
#P:\testing_data\current\CDimage_TANK_[fill in the version number]_QA
#
$tank_dir_name = "CDimage_TANK_" . $version . "_QA";
#old method:
#system ('mkdir p:\testing_data\current\CDimage_tank');
#system ('move P:\vss_db_tattoo\GPG\Out\*.* P:\testing_data\current\CDimage_tank');
#system ('rename P:\testing_data\current\CDimage_tank P:\testing_data\current\\' . $tank_dir_name);

system ('move P:\vss_db_tattoo\GPG\Out P:\testing_data\current\CDimage_tank');
system ('del P:\testing_data\current\CDimage_tank\\*.scc /q /s /f'); #get rid of vss .scc files
system ('move P:\testing_data\current\CDimage_tank P:\testing_data\current\\' . $tank_dir_name);
system ('attrib +r P:\testing_data\current\\' . $tank_dir_name . '\\* /s /d');
system ('mkdir P:\vss_db_tattoo\GPG\Out');

#________________________________________________
#
#COPY contents of:
#
#\\buildbox2\p$\vss_db_tattoo\GPG
#
#to
#
#P:\testing_data\current\CODE_[fill in the version number]_DEV
#
$code_dir_name = "CODE_" . $version . "_DEV";
system ('mkdir p:\testing_data\current\CDimage_code');
system ('xcopy \\\\buildbox2\\p$\\vss_db_tattoo\\GPG\\*.* P:\\testing_data\\current\\CDimage_code /e /q /h /k /y');
system ('del P:\testing_data\current\CDimage_code\\*.scc /q /s /f'); #get rid of vss .scc files
system ('move P:\\testing_data\\current\\CDimage_code P:\\testing_data\\current\\' . $code_dir_name);
system ('attrib +r P:\testing_data\current\\' . $code_dir_name . '\\* /s /d');
